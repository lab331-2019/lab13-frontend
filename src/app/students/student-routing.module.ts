import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsComponent } from './list/students.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { LecturerGuard } from '../guard/lecturer.guard';
import { StudentAndLecturerGuard } from '../guard/student-and-lecturer.guard';



const StudentRoutes: Routes = [
    /*
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: StudentTableComponent },
    { path: 'detail/:id', component: StudentsViewComponent },
    { path: 'table', component: StudentTableComponent }
    */
   { path: 'add', component: StudentsAddComponent, canActivate: [ LecturerGuard] },
   { path: 'list', component: StudentTableComponent, canActivate: [ StudentAndLecturerGuard] },
   { path: 'detail/:id', component: StudentsViewComponent, canActivate: [ StudentAndLecturerGuard] },
   { path: 'table', component: StudentTableComponent, canActivate: [ StudentAndLecturerGuard] }
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
